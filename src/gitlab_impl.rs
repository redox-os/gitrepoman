use gitlab::{Gitlab as GitlabApi, api::{self, Query}, types, GitlabError};
use actions::{GitAction, Repo};

pub struct Gitlab {
    client: GitlabApi,
    namespace: String,
}

impl Gitlab {
    pub fn new(host: String, namespace: String, token: String) -> Result<Self, GitlabError> {
        Ok(Self {
            client: GitlabApi::new(host, token)?,
            namespace,
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabProject {
    pub name: String,
    pub http_url_to_repo: String,
    pub ssh_url_to_repo: String,
    pub namespace: types::Namespace,
    pub default_branch: Option<String>,
}

impl GitAction for Gitlab {
    fn get_repos(&self) -> Vec<Repo> {
        let projects_query = api::paged(api::groups::projects::GroupProjects::builder()
            .group(&self.namespace)
            .build()
            .expect("failed to build projects query"),
            api::Pagination::All
        );
        let projects: Vec<GitlabProject> = projects_query.query(&self.client).expect("failed to execute projects query");

        let mut repos = Vec::new();
        for project in projects {
            repos.push(Repo {
                name: project.name,
                html_url: project.http_url_to_repo,
                ssh_url: project.ssh_url_to_repo,
                namespace: project.namespace.full_path,
                branch: project.default_branch.unwrap_or("master".to_string())
            })
        }
        repos
    }
}
